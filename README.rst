===================
cpd-sustained-cases
===================

Python package to scrape case abstracts from the `City of Chicago Independant Police Review Authority <http://www.iprachicago.org/>`_ website . 

Dependencies
============

To extract text, you'll need to have the ``pdftotext`` command available on your system.  This is part of the `XPDF tools <http://www.foolabs.com/xpdf/download.html>`_.

This is only needed if you intend to download and parse data yourself.  If you just want programmatic access to data in this package, you don't need the ``pdftotext`` utility.

Installation
============

::

    pip install git+https://ghing@bitbucket.org/ghing/cpd-sustained-cases.git

Downloading and parsing data
============================

Create a directory to store the raw and processed data::

    mkdir data

Download the pdf files::

    sustained_cases fetch --outputdir=./data

Extract text from the PDF files::

    sustained_cases extract_text ./data/*.pdf

Parse the text into CSV files.  This only works for 2014 records::

    sustained_cases parse data/abstracts_of_sustained_cases__2014*.txt


Using the data in Python
========================

::

    import sustained_cases

    for case in sustained_cases.cases:
        print(case.log_cr_no)


Using the data in a spreadsheet program
=======================================

CSV files are contained in the `data` folder in the `sustained_cases` package.  You should be able to open these files in any spreadsheet program.
