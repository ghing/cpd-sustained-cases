from datetime import datetime
import re

from sustained_cases.parser.base import ParserState, BaseParser


log_cr_no_re = re.compile(r'(?P<field>Log/C\.R\. No\.) (?P<value>\d+)')
report_month_re = re.compile(r'(?P<month>JANUARY|FEBRUARY|MARCH|APRIL|'
    'MAY|JUNE|JULY|AUGUST|SEPTEMBER|OCTOBER|NOVEMBER|DECEMBER)'
    ' (?P<year>\d{4})')
multiline_value_re = re.compile(r'(?P<field>Summary|Finding[s]{0,1}):\s*(?P<value>.*)')
multiline_summary_re = re.compile(r'On (?P<month>January|February|March|April|'
    'May|June|July|August|September|October|November|December)'
    ' (?P<day>\d+)(th|rd|)[,]{0,1} \s*(?P<year>\d{4})')


class RootState(ParserState):
    name = 'root'

    def handle_line(self, line):
        if line == "":
            return
        
        m = report_month_re.match(line)
        if m:
            self._context['Report Year'] = m.group('year')
            self._context['Report Month'] = m.group('month')
            return

        m = log_cr_no_re.search(line)
        if m:
            self._context.change_state('case', m)
            return


def parse_single_line_field(line):
    single_line_field_re = re.compile(r'(?P<field>Notification Date|'
        'Location|Complaint): (?P<value>.*)')
    m = single_line_field_re.match(line)
    if not m:
        return None, None

    val = m.group('value').strip()
    if m.group('field') == "Notification Date":
        if val == "August -November 2010":
            # HACK: Deal with wonky value
            val = "August - November, 2010"
        else:
            val = datetime.strptime(val, "%B %d, %Y").date()

    return m.group('field'), val 


class CaseState(ParserState):
    name = 'case'

    def enter(self, m=None):
        self._context.finish_case()
        if m:
            self.start_case(m.group('field'), m.group('value'))

    def handle_line(self, line):
        if line == "":
            return

        field, value = parse_single_line_field(line)
        if field:
            self._context['case'][field] = value
            return

        m = multiline_summary_re.match(line)
        if (m and self._context['Report Month'] in ("JANUARY", "FEBRUARY") and
                self._context['Report Year'] == "2014"):
            self._context.change_state('multiline_summary', m)

        m = multiline_value_re.match(line)
        if m:
            self._context.change_state('multiline_value', m)
            return

        m = log_cr_no_re.search(line)
        if m:
            self._context.finish_case()
            self.start_case(m.group('field'), m.group('value'))

    def start_case(self, field, value):
        self._context['case'] = {}
        self._context['case'][field] = value


class MultilineValueState(ParserState):
    name = 'multiline_value'

    def enter(self, m=None):
        if m:
            self.start_value(m.group('field'), m.group('value'))

    def start_value(self, field, value):
        if field == "Findings":
            field = "Finding"
        self._field = field
        self._context['case'][self._field] = value

    def handle_line(self, line):
        if line == "":
            return

        # A multiline value can span pages, so we have to ignore
        # the header/footer lines
        if "Created by INDEPENDENT POLICE REVIEW AUTHORITY" in line:
            return

        if "Abstracts of Sustained Cases" in line:
            return

        if report_month_re.match(line):
            return

        field, value = parse_single_line_field(line) 
        if field:
            self._context['case'][field] = value
            return

        m = multiline_value_re.match(line)
        if m:
            self.start_value(m.group('field'), m.group('value'))
            return

        m = log_cr_no_re.match(line)
        if m:
            self._context.change_state('case', m)
            return

        if len(self._context['case'][self._field]):
            sep = " "
        else:
            sep = ""

        self._context['case'][self._field] += sep + line


class MultilineSummaryState(ParserState):
    name = 'multiline_summary'

    def enter(self, m):
        self._field = 'Summary'
        self._context['case'][self._field] = self._context.current_line

        datestr = "{} {}, {}".format(m.group('month'), m.group('day'),
            m.group('year'))
        self._context['case']['Notification Date'] = datetime.strptime(datestr, "%B %d, %Y").date()

    def handle_line(self, line):
        if line == "":
            return

        # A multiline value can span pages, so we have to ignore
        # the header/footer lines
        if "Created by INDEPENDENT POLICE REVIEW AUTHORITY" in line:
            return

        if "Abstracts of Sustained Cases" in line:
            return

        if report_month_re.match(line):
            return

        m = log_cr_no_re.match(line)
        if m:
            self._context.change_state('case', m)
            return

        if len(self._context['case'][self._field]):
            sep = " "
        else:
            sep = ""

        self._context['case'][self._field] += sep + line


class Parser(BaseParser):
    def __init__(self):
        super(Parser, self).__init__()
        self._register_state(RootState(self))
        self._register_state(CaseState(self))
        self._register_state(MultilineValueState(self))
        self._register_state(MultilineSummaryState(self))
        self._current_state = self._get_state('root')

    def parse(self, lines):
        self['cases'] = []

        for line in lines: 
            self.raw_line = line
            clean_line = line.strip() 
            self.handle_line(clean_line)

        self.finish_case()

        return self['cases']

    def finish_case(self):
        try:
            self['case']['Report Year'] = self['Report Year']
            self['case']['Report Month'] = self['Report Month']
            self['cases'].append(self['case'])
        except KeyError:
            pass


FIELDNAMES = [
    'Report Year',
    'Report Month',
    'Log/C.R. No.',
    'Notification Date',
    'Location',
    'Complaint',
    'Summary',
    'Finding',
]
