import requests
from datetime import date
import os
import sys


def build_url(year, month):
    url_format = 'http://www.iprachicago.org/Abstracts%20of%20Sustained%20Cases_{0}.pdf'
    d = date(year, month, 1)
    return url_format.format(d.strftime('%m%Y'))

class FetchError(Exception):
    pass

def fetch(url, outfilename):
    resp = requests.get(url)
    
    if resp.status_code != requests.codes.ok:
        raise FetchError("Error requesting URL {}".format(url))

    with open(outfilename, 'w') as f:
        f.write(resp.content)

def default_filename(year, month):
    d = date(year, month, 1)
    return "abstracts_of_sustained_cases__{}.pdf".format(d.strftime("%Y%m"))

def filename_url_pairs(start_year=2008, end_year=2014):
    pairs = []

    for y in range(start_year, end_year + 1):
        for m in range(1, 13):
            url = build_url(y, m)
            outfilename = default_filename(y, m)
            pairs.append((url, outfilename))

    return pairs

def fetchall(outputdir, start_year=2008, end_year=2014):
    for url, outfilename in filename_url_pairs(start_year, end_year):
        outpath = os.path.join(outputdir, outfilename)
        if os.path.exists(outpath):
            sys.stdout.write("{} already exists. Skipping\n".format(outpath))
            continue

        try:
            sys.stdout.write("Fetching {} to {}\n".format(url, outpath))
            fetch(url, outpath)
        except FetchError as e:
            sys.stderr.write(str(e) + "\n")
