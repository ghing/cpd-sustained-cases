from collections import namedtuple
import csv
import glob
import os

csv_fields_to_attrs = {
    'Report Year': 'report_year',
    'Report Month': 'report_month',
    'Log/C.R. No.': 'log_cr_no',
    'Notification Date': 'notification_date',
    'Location': 'location',
    'Complaint': 'complaint',
    'Summary': 'summary',
    'Finding': 'finding',
}
"""
In the CSV files, the columns reflected the field names in the raw data. If we
want to use them as Python attributes, we need to rename them.
"""

Case = namedtuple('Case', 'report_year report_month log_cr_no '
    'notification_date location complaint summary finding')


def load_cases():
    cases = []
    data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)),
        'data')
    csv_filenames = os.path.join(data_dir, 'abstracts_of_sustained_cases__2014*.csv')
    for name in glob.glob(csv_filenames):
        with open(name, 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                attrs  = dict((csv_fields_to_attrs.get(k, k), v) for (k, v) in
                    row.items())
                cases.append(Case(**attrs))

    return cases

cases = load_cases()
