import os.path

from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as f:
    readme = f.read()

setup(name='sustained_cases',
      version='0.0.1',
      description=('Python package for working with sustained case abstract '
          'records'),
      long_description=readme,
      author='Geoff Hing, Wilberto Morales',
      author_email='geoffhing@gmail.com, wilbertomorales777@gmail.com',
      url='https://bitbucket.org/ghing/cpd-sustained-cases.',
      packages=['sustained_cases'],
      package_data={'sustained_cases': ['data/*.csv']},
      include_package_data=True,
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: MIT License',
          'Operating System :: OS Independent',
          'Programming Language :: Python',
          ],
     )
